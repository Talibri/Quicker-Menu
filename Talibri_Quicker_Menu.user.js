// ==UserScript==
// @name         Talibri Quicker Menu
// @namespace    http://gitlab.com/Talibri
// @version      0.2.0
// @description
// @author       Dan Campbell, original script based on work by Kaine "Pirion" Adams
// @match        https://talibri.com/*
// @grant        none
// @require  https://gist.github.com/raw/2625891/waitForKeyElements.js
// ==/UserScript==

function addQuickMenu() {
    'use strict';

    $(`
<div id="quick-menu" style="font-size: 10px; margin-left: 5px; margin-right 5px;">
<div id="quick-menu-info" class="col-md-12 row" style=" margin-top: 10px;">Quick Menu</div>
<div id="quick-menu-cities" class="col-md-12 row" style="padding-left:30px !important;">
<table class="col-md-12">
<tr>
<th width="9%"></th>
<th width="9%">Guild District</th>
<th width="9%">Enchanting</th>
<th width="9%">Cooking</th>
<th width="9%">Tailoring</th>
<th width="9%">Blacksmithing</th>
<th width="9%">Weaponsmithing</th>
<th width="9%">Woodworking</th>
<th width="9%">Alchemy</th>
<th width="9%">Construction</th>
<th width="9%">Market</th>
</tr>
<tr>
<td><a href="/cities/Gild">Gild</a></td>
<td><a href="/guild/2?id=2">Gild GD</a></td>
<td><a href="/crafting/1?city_id=1">Old Grok's Arcane Hovel</a></td>
<td><a href="/crafting/2?city_id=1">Vorla's Cook N' Clean</a></td>
<td><a href="/crafting/7?city_id=1">Leon's Workshop</a></td>
<td><a href="/crafting/12?city_id=1">Gilded Foundry</a></td>
<td><a href="/crafting/13?city_id=1">Gilded Smithy</a></td>
<td><a href="/crafting/14?city_id=1">Pelson’s Carpentry Co.</a></td>
<td><a href="/crafting/15?city_id=1">Tim’s Potions and Tonics</a></td>
<td><a href="/crafting/16?city_id=1">Billin Construction Workyard</a></td>
<td></td>
</tr>

<tr>
<td><a href="/cities/Lowenheim">Lowenheim</a></td>
<td><a href="/guild/1?id=1">Lowenheim GD</a></td>
<td><a href="/crafting/11?city_id=2">Mystra's Tower</a></td>
<td><a href="/crafting/9?city_id=2">Purple Unicorn, Popular Tavern and Inn</a></td>
<td><a href="/crafting/10?city_id=2">Mane's Thread</a></td>
<td><a href="/crafting/3?city_id=2">Koronis' Foundry</a></td>
<td><a href="/crafting/4?city_id=2">Koronis' Smithy</a></td>
<td><a href="/crafting/5?city_id=2">Pel's Woodworking Shop</a></td>
<td><a href="/crafting/6?city_id=2">Old Grok's Alchemy Lab</a></td>
<td><a href="/crafting/8?city_id=2">Khandri's Construction Yard</a></td>
<td><a href="/trade/1">Lowenheim Market District</a></td>
</tr>

<tr>
<td><a href="/cities/Emelglad">Emelglad</a></td>
<td><a href="/guild/3?id=3">Emelglad</a></td>
<td><a href="/crafting/19?city_id=3">Oronin’s Glow</a></td>
<td><a href="/crafting/17?city_id=3">Hassan’s Respite</a></td>
<td><a href="/crafting/18?city_id=3">Evergloam Threads</a></td>
<td><a href="/crafting/20?city_id=3">Nature's Flame</a></td>
<td><a href="/crafting/21?city_id=3">Nature’s Hammer</a></td>
<td><a href="/crafting/22?city_id=3">Sawdogs</a></td>
<td><a href="/crafting/23?city_id=3">Shady Corner</a></td>
<td><a href="/crafting/24?city_id=3">Forest's Edge Lumber Mill</a></td>
<td></td>
</tr>

<tr>
<td><a href="/cities/Cauruin">Cauruin</a></td>
<td><a href="/guild/4?id=4">Cauruin GD</a></td>
<td><a href="/crafting/27?city_id=4">Oronin’s Glow</a></td>
<td><a href="/crafting/25?city_id=4">Blackstone Tavern</a></td>
<td><a href="/crafting/26?city_id=4">Nine Saved</a></td>
<td><a href="/crafting/28?city_id=4">Warbringer</a></td>
<td><a href="/crafting/29?city_id=4">Warbreaker</a></td>
<td><a href="/crafting/30?city_id=4">Vinius Woodworking Supplies</a></td>
<td><a href="/crafting/31?city_id=4">Helda’s Favorite Flask</a></td>
<td><a href="/crafting/32?city_id=4">Vinius Quarry</a></td>
<td></td>
</tr>

<tr>
<td><a href="/cities/Necropolis">Necropolis</a></td>
<td><a href="/guild/5?id=5">Necropolis GD</a></td>
<td><a href="/crafting/35?city_id=5">Dobrogost's Charms</a></td>
<td><a href="/crafting/33?city_id=5">The Long Sleep Inn</a></td>
<td><a href="/crafting/34?city_id=5">Cinches and Stitches</a></td>
<td><a href="/crafting/36?city_id=5">The Blade's Beginning</a></td>
<td><a href="/crafting/37?city_id=5">The Blade’s End</a></td>
<td><a href="/crafting/38?city_id=5">Camille’s Carpentry</a></td>
<td><a href="/crafting/39?city_id=5">The Last Drop</a></td>
<td><a href="/crafting/40?city_id=5">Altan Quarry</a></td>
<td></td>
</tr>

</table>
<p>&nbsp;</p>
</div>


<div id="quick-menu-gathering" class="col-md-12 row">
<p class="col-md-1">Gathering</p>
<p class="col-md-11">
<a href="https://talibri.com/locations/1/show">Erets Forest</a>
| <a href="https://talibri.com/locations/2/show">Evergloam</a>
| <a href="https://talibri.com/locations/3/show">Gloomglade</a>
| <a href="https://talibri.com/locations/4/show">Mikhail Mountain</a>
| <a href="https://talibri.com/locations/5/show">Enmuyama Mountain</a>
| <a href="https://talibri.com/locations/6/show">Dragonmount</a>
| <a href="https://talibri.com/locations/7/show">Glaurduin</a>
| <a href="https://talibri.com/locations/8/show">Glittering Coast</a>
| <a href="https://talibri.com/locations/9/show">Sea of Berseer</a>
| <a href="https://talibri.com/locations/10/show">Veran Plains</a>
| <a href="https://talibri.com/locations/11/show">Sea of Glass</a>
| <a href="https://talibri.com/locations/12/show">Andaelin Lake</a>
| <a href="https://talibri.com/locations/13/show">Dorran Mines</a>
</p>
</div>

<div id="quick-menu-combat" class="col-md-12 row">
<p class="col-md-1">Combat</p>
<p class="col-md-11">
<a href="https://talibri.com/combat_zones/3/adventure">Calin Beach</a>
| <a href="https://talibri.com/combat_zones/1/adventure">Fields of Paelin</a>
| <a href="https://talibri.com/combat_zones/2/adventure">Spider Cave</a>
| <a href="https://talibri.com/combat_zones/4/adventure">Ruins of Morgaroth</a>
| <a href="https://talibri.com/combat_zones/6/adventure">The Tower of Ice</a>
| <a href="https://talibri.com/combat_zones/5/adventure">Abyssal Rift</a>
| <a href="https://talibri.com/combat_zones/7/adventure">Fulgur Deep</a>
| <a href="https://talibri.com/combat_zones/8/adventure">Lowenheim Outskirts</a>
| <a href="https://talibri.com/combat_zones/9/adventure">Ariz Desert</a>
| <a href="https://talibri.com/combat_zones/10/adventure">Dead Scar - South</a>
</p>
</div>

</div>`).prependTo('.main-page');

}


waitForKeyElements(".main-page", addQuickMenu);